package de.mlh1100.PathFollower;

import de.mlh1100.PathFollower.Commands.CreateRouteCMD;
import de.mlh1100.PathFollower.Commands.ListCornersCMD;
import de.mlh1100.PathFollower.Commands.SetCornerCMD;
import de.mlh1100.PathFollower.Commands.SpawnFollower;
import de.mlh1100.PathFollower.Listeners.PlayerInteractListener;
import lombok.Getter;
import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PathFollower extends JavaPlugin {

    private static @Getter PathFollower pathFollower;
    private @Getter YamlConfiguration cfg;

    private @Getter List<Entity> follower;
    private @Getter Map<Entity, Integer> followerRouteMap;

    public PathFollower() {

        pathFollower = this;
        this.follower = new ArrayList<>();
        this.followerRouteMap = new HashMap<>();

        File file = new File(getDataFolder(), "config.yml");
        if(!file.exists()){
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        this.cfg = YamlConfiguration.loadConfiguration(file);

    }

    @Override
    public void onEnable() {

        setupCfg();
        registerCommands();
        registerListeners();

        respawnAllFollower();

        getLogger().info("§aPathFollower wurde erflogreich gestartet");

    }

    @Override
    public void onDisable() {

        saveAllFollowerLocations();

        getLogger().info("§aPathFollower wurde erflogreich gestopt");

    }

    private void registerListeners() {
        Bukkit.getPluginManager().registerEvents(new PlayerInteractListener(), this);
    }

    /**
     * Registers all Commands
     */
    private void registerCommands() {
        Bukkit.getPluginCommand("createroute").setExecutor(new CreateRouteCMD());
        Bukkit.getPluginCommand("listcorners").setExecutor(new ListCornersCMD());
        Bukkit.getPluginCommand("sercorner").setExecutor(new SetCornerCMD());
        Bukkit.getPluginCommand("spawnfoller").setExecutor(new SpawnFollower());
    }

    /**
     * Setting up config.yml defaults
     */
    private void setupCfg() {

        cfg.addDefault("Routes.Count", 0);

        cfg.addDefault("Routes.'1'.Name", "");
        cfg.addDefault("Coordinates.'1'.'1'.X", "");
        cfg.addDefault("Coordinates.'1'.'1'.Y", "");
        cfg.addDefault("Coordinates.'1'.'1'.Z", "");

        cfg.addDefault("Coordinates.Follower.'1'.Type", "");
        cfg.addDefault("Coordinates.Follower.'1'.Route", "");
        cfg.addDefault("Coordinates.Follower.'1'.X", "");
        cfg.addDefault("Coordinates.Follower.'1'.Y", "");
        cfg.addDefault("Coordinates.Follower.'1'.Z", "");

        cfg.options().copyDefaults(true);
        saveConfig();

    }

    /**
     * Method to respawn the all Followers at their old location and continue their route walking
     */
    public void respawnAllFollower() {

    }

    /**
     * Method to save the Followers location for the next start
     */
    public void saveAllFollowerLocations() {
        int[] count = {1};
        this.follower.forEach(follower -> {
            Location loc = follower.getLocation();
            cfg.set("Coordinates.Follower.'" + String.valueOf(count[0]) + "'.X", (int) loc.getX());
            cfg.set("Coordinates.Follower.'" + String.valueOf(count[0]) + "'.Y", (int) loc.getY());
            cfg.set("Coordinates.Follower.'" + String.valueOf(count[0]) + "'.Z", (int) loc.getZ());
            ++count[0];
        });
        saveConfig();
    }

    /**
     * Method to check if the passed route exists and return the numeric route identifier
     *
     * @param routeIdentifier needs the name of the route
     * @return returns the numeric route identifier
     */
    public int identifyRoute(String routeIdentifier) {
        int routeIndex;
        /* Checking if the route identifier is a digit */
        /* else searching for the string */
        int routeCount = cfg.getInt("Routes.Count");
        if(!StringUtils.isNumeric(routeIdentifier)) {
            List<String> routes = new ArrayList<>();

            /* Querying every routes name */
            for (int i = 0; i <= routeCount; i++) {
                routes.add(cfg.getString("Routes.'" + routeCount + "'.Name"));
            }
            /* and search in the list for their numeric identifier */
            if (routes.contains(routeIdentifier)) {
                routeIndex = routes.indexOf(routeIdentifier);
            } else {
                return -1;
            }

        } else {
            /* just parseing the String into an integer */
            routeIndex = Integer.parseInt(routeIdentifier);

            /* Checking if the route exists */
            if(routeIndex > routeCount) {
                return -1;
            }
        }

        return routeIndex;
    }

}
