package de.mlh1100.PathFollower.EntityManagment.CostumEntities;

import net.minecraft.server.v1_12_R1.EntityPig;
import net.minecraft.server.v1_12_R1.PathfinderGoalFloat;
import net.minecraft.server.v1_12_R1.PathfinderGoalSelector;
import net.minecraft.server.v1_12_R1.World;

import java.lang.reflect.Field;
import java.util.List;

public class CostumPig extends EntityPig {

    public CostumPig(World world, int route) {
        super(world);

        List goalB = (List) getPrivateField("b", PathfinderGoalSelector.class, goalSelector);
        goalB.clear();

        List goalC = (List)getPrivateField("c", PathfinderGoalSelector.class, goalSelector);
        goalC.clear();

        List targetB = (List)getPrivateField("b", PathfinderGoalSelector.class, targetSelector);
        targetB.clear();

        List targetC = (List)getPrivateField("c", PathfinderGoalSelector.class, targetSelector);
        targetC.clear();

        this.goalSelector.a(0, new PathfinderGoalFloat(this));
    }

    public CostumPig(World world, int route, int corner) {
        super(world);

        List goalB = (List) getPrivateField("b", PathfinderGoalSelector.class, goalSelector);
        goalB.clear();

        List goalC = (List)getPrivateField("c", PathfinderGoalSelector.class, goalSelector);
        goalC.clear();

        List targetB = (List)getPrivateField("b", PathfinderGoalSelector.class, targetSelector);
        targetB.clear();

        List targetC = (List)getPrivateField("c", PathfinderGoalSelector.class, targetSelector);
        targetC.clear();

        this.goalSelector.a(0, new PathfinderGoalFloat(this));

    }


    private Object getPrivateField(String fieldName, Class clazz, Object object)
    {
        Field field;
        Object o = null;

        try {
            field = clazz.getDeclaredField(fieldName);

            field.setAccessible(true);

            o = field.get(object);
        }
        catch(NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        return o;
    }
}
