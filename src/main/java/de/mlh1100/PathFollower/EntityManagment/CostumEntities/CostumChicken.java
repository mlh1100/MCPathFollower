package de.mlh1100.PathFollower.EntityManagment.CostumEntities;

import net.minecraft.server.v1_12_R1.EntityChicken;
import net.minecraft.server.v1_12_R1.PathfinderGoalSelector;
import net.minecraft.server.v1_12_R1.World;

import java.lang.reflect.Field;
import java.util.List;

public class CostumChicken extends EntityChicken {


    public CostumChicken(World world) {
        super(world);

        List goalB = (List)getPrivateField("b", PathfinderGoalSelector.class, goalSelector);
        goalB.clear();

        List goalC = (List)getPrivateField("c", PathfinderGoalSelector.class, goalSelector);
        goalC.clear();

        List targetB = (List)getPrivateField("b", PathfinderGoalSelector.class, targetSelector);
        targetB.clear();

        List targetC = (List)getPrivateField("c", PathfinderGoalSelector.class, targetSelector);
        targetC.clear();

    }

    private Object getPrivateField(String fieldName, Class clazz, Object object)
    {
        Field field;
        Object o = null;
        try
        {
            field = clazz.getDeclaredField(fieldName);
            field.setAccessible(true);
            o = field.get(object);
        }
        catch(NoSuchFieldException | IllegalAccessException e)
        {
            e.printStackTrace();
        }
        return o;
    }

}
