package de.mlh1100.PathFollower.EntityManagment;

import de.mlh1100.PathFollower.EntityManagment.CostumEntities.CostumChicken;
import de.mlh1100.PathFollower.EntityManagment.CostumEntities.CostumHorse;
import de.mlh1100.PathFollower.EntityManagment.CostumEntities.CostumPig;
import net.minecraft.server.v1_12_R1.Entity;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_12_R1.CraftWorld;

import java.lang.reflect.Field;
import java.util.Map;

public enum CostumEntityTypes {

    COSTUM_PIG("Pig", 54, CostumPig.class),
    COSTUM_CHICKEN("Chicken", 55, CostumChicken.class),
    COSTUM_HORSE("Horse", 56, CostumHorse.class);

    private CostumEntityTypes(String name, int id, Class<? extends Entity> costum) {
        addToMaps(costum, name, id);
    }

    public static void spawnEntity(Entity entity, Location loc) {
        entity.setLocation(loc.getX(), loc.getY(), loc.getZ(), loc.getYaw(), loc.getPitch());
        ((CraftWorld)loc.getWorld()).getHandle().addEntity(entity);
    }

    private static void addToMaps(Class clazz, String name, int id) {
        ((Map)getPrivateField("c",
                net.minecraft.server.v1_12_R1.EntityTypes.class,
                null)).put(name, clazz);
        ((Map)getPrivateField("d",
                net.minecraft.server.v1_12_R1.EntityTypes.class,
                null)).put(clazz, name);
        ((Map)getPrivateField("f",
                net.minecraft.server.v1_12_R1.EntityTypes.class,
                null)).put(clazz, Integer.valueOf(id));
    }

    private static Object getPrivateField(String fieldName, Class<net.minecraft.server.v1_12_R1.EntityTypes> clazz, Object object) {
        Field field;
        Object o = null;

        try {
            field = clazz.getDeclaredField(fieldName);

            field.setAccessible(true);

            o = field.get(object);
        }
        catch(NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        return o;
    }

}
