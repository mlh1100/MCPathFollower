package de.mlh1100.PathFollower.EntityManagment;

import de.mlh1100.PathFollower.PathFollower;
import net.minecraft.server.v1_12_R1.EntityInsentient;
import net.minecraft.server.v1_12_R1.NavigationAbstract;
import net.minecraft.server.v1_12_R1.PathEntity;
import net.minecraft.server.v1_12_R1.PathfinderGoal;
import org.bukkit.Location;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Entity;

public class PathfinderGoalWalkRoute extends PathfinderGoal {

    private YamlConfiguration cfg;

    private double speed;
    private EntityInsentient entityInsentient;
    private Entity entity;
    private Location loc;
    private NavigationAbstract nav;
    private int route;
    private int corner;

    public PathfinderGoalWalkRoute(double speed, EntityInsentient entityInsentient, NavigationAbstract nav,
                                   int route, Entity entity) {
        PathFollower pf = PathFollower.getPathFollower();
        this.cfg = pf.getCfg();
        this.speed = speed;
        this.entityInsentient = entityInsentient;
        this.nav = nav;
        this.route = route;
        this.entity = entity;
    }

    public PathfinderGoalWalkRoute(double speed, EntityInsentient entityInsentient, NavigationAbstract nav,
                                   int route, int corner, Entity entity) {
        this.speed = speed;
        this.entityInsentient = entityInsentient;
        this.nav = nav;
        this.route = route;
        this.corner = corner;
        this.entity = entity;
    }

    /**
     *
     * @return Returns always true because it should execute c() on every spawn
     */
    @Override
    public boolean a() {
        return true;
    }

    @Override
    public void c() {
        //TODO Repeat for each corner
        /* Querying all corner-coords to make the code more efficient */
        int cornerX = cfg.getInt("Coordinate.'" +
                String.valueOf(this.route) + "'.'" +
                String.valueOf(this.corner) + "'.X");
        int cornerY = cfg.getInt("Coordinate.'" +
                String.valueOf(this.route) + "'.'" +
                String.valueOf(this.corner) + "'.Z");
        int cornerZ = cfg.getInt("Coordinate.'" +
                String.valueOf(this.route) + "'.'" +
                String.valueOf(this.corner) + "'.Z");

        /* Checking if the distance to the next corner is lower equal 20
           because the Navigation class has a maximum range of 20 Blocks */
        while ((((this.entity.getLocation().getX() - cornerX) > 20) || ((this.entity.getLocation().getX() - cornerX) < -20)) &&
                (((this.entity.getLocation().getX() - cornerY) > 20) || ((this.entity.getLocation().getX() - cornerY) < -20)) &&
                (((this.entity.getLocation().getX() - cornerZ > 20) || ((this.entity.getLocation().getX() - cornerZ) < -20)))) {

            PathEntity pathEntity = this.nav.a(this.getCountToSubtract((int) this.entity.getLocation().getX()),
                    this.getCountToSubtract((int) this.entity.getLocation().getY()),
                    this.getCountToSubtract((int) this.entity.getLocation().getZ()));
            this.nav.a(pathEntity, speed);

        }


        PathEntity pathEntity = this.nav.a(cornerX,cornerY,cornerZ);
        this.nav.a(pathEntity, speed);
    }

    /**
     *
     * @param number the number which should get 20
     * @return returns the number which must be subtracted to be 20
     */
    private int getCountToSubtract(int number) {
        if(number <= 20) {
            return 0;
        } else {
            int toSubtract = 0;
            while (number > 20) {
                number--;
                ++toSubtract;
            }
            return toSubtract;
        }
    }
}
