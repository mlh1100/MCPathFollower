package de.mlh1100.PathFollower.Commands;

import de.mlh1100.PathFollower.PathFollower;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.*;

public class SpawnFollower implements CommandExecutor {

    private PathFollower pf;
    private YamlConfiguration cfg;

    public SpawnFollower() {
        this.pf = PathFollower.getPathFollower();
        this.cfg = pf.getCfg();
    }

    /**
     * CommandParam: [Entity type] [route identifier] <Entity name>
     *
     * @return Execution success
     */
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String lable, String[] args) {
        if (!(sender instanceof Player)) {
            /* Check if the executor is a Player to prevent errors */
            pf.getLogger().info("You have to be a Player to execute this command!");
            return true;
        }

        Player p = (Player) sender;

        if(args.length <= 1) {
            /* No args = no entity spawn */
            p.sendMessage(Color.RED.toString() + "Too less arguments!");
            p.sendMessage(Color.RED.toString() + "/spawnfollower [Entity type] [route] <Entity name>");
            return true;
        } else if(args.length >= 2) {
            int routeIndex = pf.identifyRoute(args[1]);
            if(routeIndex == -1) {
                p.sendMessage(Color.RED.toString() + "This route doesn't exist! Please pass an valid route identifier!");
                return true;
            }

            /* Spawning the new Follower at the first */
            LivingEntity follower = null;
            Location spawnLocation = new Location( p.getWorld(),
                    cfg.getInt("Coordinates.'" + String.valueOf(routeIndex) + "'.'1'.X"),
                    cfg.getInt("Coordinates.'" + String.valueOf(routeIndex) + "'.'1'.Z"),
                    cfg.getInt("Coordinates.'" + String.valueOf(routeIndex) + "'.'1'.X"));
            switch (args[0]) {
                case "Pig":
                    follower = (Pig) p.getWorld().spawnEntity(spawnLocation, EntityType.PIG);
                    break;
                case "Chicken":
                    follower = (Chicken) p.getWorld().spawnEntity(spawnLocation, EntityType.CHICKEN);
                    break;
                case "Horse":
                    follower = (Horse) p.getWorld().spawnEntity(spawnLocation, EntityType.HORSE);
                    break;
            }
            pf.getFollower().add(follower);
            pf.getFollowerRouteMap().put(follower, routeIndex);

            if ((follower != null) && (args.length == 3)) {
                follower.setCustomName(args[2]);
                follower.setCustomNameVisible(true);
            }



        }

        return false;
    }
}
