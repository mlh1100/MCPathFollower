package de.mlh1100.PathFollower.Commands;

import de.mlh1100.PathFollower.PathFollower;
import org.bukkit.Color;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

public class ListCornersCMD implements CommandExecutor {

    private PathFollower pf;
    private YamlConfiguration cfg;

    public ListCornersCMD() {
        this.pf = PathFollower.getPathFollower();
        this.cfg = pf.getCfg();
    }

    /**
     * CommandParam: [route identifier]
     *
     * @return Execution success
     */
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (!(sender instanceof Player)) {
            /* Check if the executor is a Player to prevent errors */
            pf.getLogger().info("You have to be a Player to execute this command!");
            return true;
        }

        Player p = (Player) sender;

        if(args.length == 0) {
            /* No args = list of corners */
            p.sendMessage(Color.RED.toString() + "Please pass the route's name!");
            p.sendMessage(Color.RED.toString() + "/listcorners [route identifier]");
            return true;
        } else if (args.length == 1) {
            int routeIndex = pf.identifyRoute(args[1]);
            if(routeIndex == -1) {
                p.sendMessage(Color.RED.toString() + "This route doesn't exist! Please pass an valid route identifier!");
                return true;
            }

            p.sendMessage(Color.LIME.toString() + "These are all corners for the route " + args[0]);
            for (int i = 0; i < 20; i++) {
                p.sendMessage(Color.LIME.toString() + "Corner " + String.valueOf(i) +
                        ": X:" + cfg.getString("Coordinates.'" + String.valueOf(routeIndex) + "'.X") +
                        "; Y:" + cfg.getString("Coordinates.'" + String.valueOf(routeIndex) + "'.Y") +
                        "; Z:" + cfg.getString("Coordinates.'" + String.valueOf(routeIndex) + "'.X"));
            }

        } else {
            /* only one route at the time */
            p.sendMessage(Color.RED.toString() + "Too many arguments!");
            p.sendMessage(Color.RED.toString() + "/listcorners [route identifier]");
            return true;
        }

        return false;
    }
}
