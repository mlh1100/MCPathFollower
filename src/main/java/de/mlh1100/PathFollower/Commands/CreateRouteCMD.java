package de.mlh1100.PathFollower.Commands;

import de.mlh1100.PathFollower.PathFollower;
import org.bukkit.Color;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

public class CreateRouteCMD implements CommandExecutor {

    private PathFollower pf;
    private YamlConfiguration cfg;

    public CreateRouteCMD() {
        this.pf = PathFollower.getPathFollower();
        this.cfg = pf.getCfg();
    }

    /**
     * Method that fires when the command is executed by Player or Console
     * CommandParam: [route name]
     *
     * @return Execution success
     */
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (!(sender instanceof Player)) {
            /* Check if the executor is a Player to prevent errors */
            pf.getLogger().info("You have to be a Player to execute this command!");
            return true;
        }

        Player p = (Player) sender;

        if (args.length == 0) {
            /* No args = no new route */
            p.sendMessage(Color.RED.toString() + "Please pass a name of the new route!");
            p.sendMessage(Color.RED.toString() + "/createroute [route name]");
            return true;
        } else if (args.length == 1) {
            /* Saving the name into the config file */
            cfg.set("Routes.'" + (String.valueOf(cfg.getInt("Routes.Coubnt") + 1)) + "'.Name", args[0]);
            p.sendMessage(Color.LIME.toString() + "The route " + args[0] + " was successfully created!");
            return true;
        } else {
            /* route name can only be one word */
            p.sendMessage(Color.RED.toString() + "Too many arguments!");
            p.sendMessage(Color.RED.toString() + "/createroute [route name]");
            return true;
        }
    }
}
