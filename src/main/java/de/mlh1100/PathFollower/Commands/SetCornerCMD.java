package de.mlh1100.PathFollower.Commands;

import de.mlh1100.PathFollower.PathFollower;
import org.apache.commons.lang.StringUtils;
import org.bukkit.Color;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

public class SetCornerCMD implements CommandExecutor {

    private PathFollower pf;
    private YamlConfiguration cfg;

    public SetCornerCMD() {
        this.pf = PathFollower.getPathFollower();
        this.cfg = pf.getCfg();
    }

    /**
     * Method that fires when the command is executed by Player or Console
     * CommandParam setting at PlayersLoc: [routeIdentifier] [chronological count]
     * CommandParam setting at other cords: [routeIdentifier] [chronological count]
     *      <x-Coordinate> <y-Coordinate> <z-Coordinate>
     *
     * @return Execution success
     */
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) {
            /* Check if the executor is a Player to prevent errors */
            pf.getLogger().info("You have to be a Player to execute this command!");
            return true;
        }

        Player p = (Player) sender;

        /* Check for the count of given arguments to identify which coordinates to set */
        if ((args.length == 0) || (args.length == 1)) {
            /* No args = no cords set */
            /* Only route = no cords set */
            p.sendMessage(Color.RED.toString() + "/setcorner [route identifier] [chronological count]" +
                    " <x-Coordinate> <y-Coordinate> <z-Coordinate>");
            return true;
        } else if (args.length == 2) {
            if (!StringUtils.isNumeric(args[1])) {
                /* Not numeric = not a valid count */
                p.sendMessage(Color.RED.toString() + "The chronological must be a digit!");
                return true;
            }
            /* Saving the cords in the config file */
            cfg.set("Coordinates." + args[0] + ".'" + args[1] + "'.X:", p.getLocation().getBlockX());
            cfg.set("Coordinates." + args[0] + ".'" + args[1] + "'.Y:", p.getLocation().getBlockY());
            cfg.set("Coordinates." + args[0] + ".'" + args[1] + "'.Z:", p.getLocation().getBlockZ());

            p.sendMessage(Color.LIME.toString() + "Corner " + args[1] + " of route " + args[0] +
                    " successfully set!");
            return true;
        } else if ((args.length > 2) && (args.length < 5)) {
            /* No complete location = no set cords */
            p.sendMessage(Color.RED.toString() + "Please pass a full location (with x, y & z)!");
            p.sendMessage(Color.RED.toString() + "/setcorner [route identifier] [chronological count]" +
                    " <x-Coordinate> <y-Coordinate> <z-Coordinate>");
            return true;
        } else if (args.length == 5) {
            if ((!StringUtils.isNumeric(args[1]))) {
                /* Not numeric = not a failed count */
                p.sendMessage(Color.RED.toString() + "The chronological must be a digit!");
                return true;
            } else if ((!StringUtils.isNumeric(args[2])) || (!StringUtils.isNumeric(args[3])) || (!StringUtils.isNumeric(args[4]))) {
                p.sendMessage(Color.RED.toString() + "The coordinates must be digits!");
                return true;
            }
            /* Saving the cords in the config file */
            cfg.set("Coordinates." + args[0] + ".'" + args[1] + "'.X:", args[2]);
            cfg.set("Coordinates." + args[0] + ".'" + args[1] + "'.Y:", args[3]);
            cfg.set("Coordinates." + args[0] + ".'" + args[1] + "'.Z:", args[4]);

            p.sendMessage(Color.LIME.toString() + "Corner " + args[1] + " of route " + args[0] +
                    " successfully set!");
            return true;
        } else if (Integer.parseInt(args[1]) > 20) {
            p.sendMessage(Color.RED.toString() + "20 is the maximum amount of corners!");
            return true;
        } else {
            p.sendMessage(Color.RED.toString() + "Too many arguments!");
            p.sendMessage(Color.RED.toString() + "/setcorner [route identifier] [chronological count]" +
                    " <x-Coordinate> <y-Coordinate> <z-Coordinate>");
            return true;
        }

    }
}
